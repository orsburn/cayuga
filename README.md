# README

## Contents

* What it is (short)
* What it is (long)
* Documentation
* Examples

## What it is (short)

* A JavaScript library to request server rendered page elements which are "hot swapped" on the page in response to a DOM event.
* A PHP library to respond to application requests for server rendered content.

## What it is (long)

Another JavaScript front-end framework. Sort of. The plethora of JS frameworks and libraries try to pass off their existence by way of an acknowledgment that they're just one more of such, being thrown on the mountain of others.  And while the humility of such introspection is appreciated, the philosophical differences that lead to each these new projects can rarely justify their existence.

Cayuga is really trying to be different though, in that, it's truly built for server-side rendering. It doesn't initially render the elements on the page upon first run, and then update future modules in the client. Instead, it actually renders all modules on the server, and passes the fully rendered element to the client—all the time.

In order for a front-end library to load a module into the page, it needs to load data from the server to populate the module with, first. Since the interaction with the server is happening already, it's a trivial amount of more data to send the rendered element instead of just the data.

Cayuga is a JavaScript object identifying the page elements to watch for events. Then, the handling of these events can be passed back to the server for processing. There is one entry in the object for each such element. Or, there can be an entry for each type of event the element should be bound to.

Finally, there's the library that implements those definitions. It's all kicked-off with a single instance of Cayuga.

## Getting Started

>It should be noted that there are several ways to implement the handlers for your user interface. The steps detailed here are just one, but should serve as a good starting point.

There are five steps to add Cayuga to an application.

1. Install Cayuga.
1. Add the Cayuga entry point into the application.
1. Add event handlers.
1. Include the Cayuga JavaScript.
1. Define a Cayuga JavaScript instance.

### Step 1

Installation should be handled through Composer.

```composer install orsburn/cayuga```

### Step 2

Place a PHP program at the root of your application named ```cayuga.php```. This is the entry point for the server-side UI content, and is where the handlers for your UI are initialized.

It should instantiate your UI handlers.

```php
<?php

include_once __DIR__ . "/vendor/autoload.php";

$cayuga = new \Cayuga\Handler(
	// Handlers for elements with events against
	// the class attribute.
	new Application\CayugaHandlers\HtmlClass(),

	// Handlers for elements with events against
	// the id attribute.
	new Application\CayugaHandlers\HtmlId()
);
```

### Step 3

Create a directory named something like ```src/CayugaHandlers```. In this directory create the classes to handle the HTML events from your application. For example:

```
src/CayugaHandlers/HtmlClass.php
src/CayugaHandlers/HtmlId.php
```

### Step 4

Drop the ```cayuga.min.js``` script wherever the JavaScript in your application lives, and point your HTML to it. For example:

```html
<script src="client/js/cayuga.min.js"></script>
```

### Step 5

Create a ```<script>``` block at the bottom of your HTML, something like this:

```html
<script>
new Cayuga({
	"#login": {
		event: "click",
		outlet: "#outlet",
		fields: ["username", "password"]
	}
},
"http://localhost/application/cayuga.php");
</script>
```

## Examples

> The examples are currently undergoing a significant re-write, so they should not be depended on.

In addition to the docs, there is an example of how Cayuga works available in __examples/index.html__.

http://server.domain.tld/cayuga/examples/index.html

1. Copy the example directory to a webserver.
1. Make sure the **script** **src** attribute points to the correct server information. It's currently:  http://localhost:8989.

## Documentation

The documentation is well underway, but is still a work in progress.  What's more, is that it's currently only available as ODT or PDF files. The expectation is that it will be duplicated into a browser-friendly version as well.

* **Cayuga (client).pdf** is for the client-side library.
* **Cayuga (server).pdf** is for the server-side UI rendering.

## Detached UI

In simple terms, the UI is simple thought of as an embedded portion of the application, that can be easily added as needed, with a folder of classes, some templates or views, and the cayuga.php entry point. The reality is, that the UI can easily be broken out into an application of it's own.

The structure described in above can be put into a stand along application that does noting but serve UI content. Just be sure to point the second parameter of the Cayuga() instantiation to the UI URL.
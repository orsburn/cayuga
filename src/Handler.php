<?php

/* =============================================================================
Supported methods of implementing the event handlers.

Assume a CayugaJS element selector named #lastName.

---------------------------------------------------- Single Class Implementation

class CayugaHandler extends Cayuga\Handler

The idea is that an HTML selector will map to a method on the class to handle
client-side events.

Assuming an HTML selector of #lastName, the Cayuga handler can be implemented
with a method named CayugaHandler->lastName().

If an HTML element selector is also present named .lastName, then
CayugaHandler->lastName() is no longer sufficient because it would be fired in
response to both #lastName and .lastName.

Cayuga has support for this by allowing the handlers to be prefixed with the
type of selector in the HTML, class, id, tags and attribute. In this example,
there would be:

	CayugaHandler->idLastName()
	CayugaHandler->classLastName()

-------------------------------------------------- Multiple Class Implementation

A way to organize handler implementations could be to break them down by
selector type (class, id, tag, and attribute). Doing so is for organization
only since the handlers are invoked in the same way as if they were all defined
in a single class that extends the main Cayuga class.

	class ClassHandlers extends Cayuga\Handler
	class IdHandlers extends Cayuga\Handler
	class TagHandlers extends Cayuga\Handler
	class AttributeHandlers extends Cayuga\Handler

There's nothing from preventing the following situation.

	IdHandler->lastName()
	ClassHandler->lastName()

PHP does not allow this, so the solution is to do the same thing as if all the
handlers were in the same class.

	IdHandler->idLastName()
	ClassHandler->classLastName()

This approach would require each selector type to be instantiated in order to be
known by the Cayuga instance they extend. For example:

	new ClassHandlers(); // This technique needs to be vetted.
	new IdHandlers();

A final implementation is to define the handlers in separate classes, but pass
instances of them when instantiating the Cayuga instance.

$cayuga = new Cayuga\Handler(
	new ClassHandlers(),
	new IdHandlers(),
	new TagHandlers(),
	new AttributeHandlers()
);

Organized a similar way, may looks like this:

$cayuga = new Cayuga\Handler(
	new handlers\ClassHandlers(),
	new handlers\IdHandlers(),
	new handlers\TagHandlers(),
	new handlers\AttributeHandlers()
);
============================================================================= */

namespace Cayuga;

class Handler
{
	public bool $instanceOfCayuga = true;
	public ?string $selectorType = null;
	public ?string $selectorName = null;
	public null|string|\stdClass $payload = null;
	public ?\stdClass $variables = null; // If the payload is form data, the fields will be populated here.
	public ?\stdClass $eventSource = null; // The attributes of the event source element.
	protected array $allowOrigin = [];
	protected string $requestMethod;
	protected ?string $js = null; // JavaScript is stored here until it can be sent with the rest of the response.
	protected ?array $headers = [];
	protected ?object $classClass;
	protected ?object $idClass;
	protected ?object $tagClass;
	protected ?object $attributeClass;
	protected bool $definedInSeparateClass = false;
	protected bool $definedJsInSeparateClass = false;
	protected ?string $methodName = null;
	protected ?string $methodNameJs = null;
	public bool $foundHandlerMethod = false;

	public function __construct(
		?object $classClass = null,
		?object $idClass = null,
		?object $tagClass = null,
		?object $attributeClass = null
	)
	{
		$this->classClass = $classClass;
		$this->idClass = $idClass;
		$this->tagClass = $tagClass;
		$this->attributeClass = $attributeClass;
		$this->requestMethod = strtoupper($_SERVER["REQUEST_METHOD"] ?? "");

		$this->populatePayload();
		$this->populateEventSource();

		$this->methodName
			= !is_null($this->selectorType)
			? $this->fetchHandlerMethodName($this->selectorType)
			: null;

		$this->methodNameJs
			= !is_null($this->selectorType)
			? $this->fetchHandlerMethodName($this->selectorType, true)
			: null;

		/*
		If the handler classes weren't passed in on instantiation, then they're assumed
		to have been defined in separate classes, and need to be invoked with the static
		run() method. Otherwise, we're going to go ahead and handle the call now.
		*/
		if(
			!is_null($this->classClass()) ||
			!is_null($this->idClass()) ||
			!is_null($this->tagClass()) ||
			!is_null($this->attributeClass())
		)
		{
			$this->handle();
		}
	}

	/**
	 * Find all Cayuga instances and run their handle() methods.
	 */
	public static function run() : bool
	{
		$variablesNames = array_keys($GLOBALS);
		$haveFoundHandlerMethod = false;
		$selectorType = null;
		$selectorName = null;

		foreach($variablesNames as $variableName)
		{
			if(
				is_object($GLOBALS[$variableName]) &&
				isset($GLOBALS[$variableName]->instanceOfCayuga) // No need to check the value, that it exists is enough.
			)
			{
				$selectorType = $GLOBALS[$variableName]->selectorType;
				$selectorName = $GLOBALS[$variableName]->selectorName;

				if(
					isset($GLOBALS[$variableName]->foundHandlerMethod) &&
					($GLOBALS[$variableName]->foundHandlerMethod === true)
				)
				{
					$haveFoundHandlerMethod = true;
					$selectorType = $GLOBALS[$variableName]->selectorType;
					$selectorName = $GLOBALS[$variableName]->selectorName;

					$GLOBALS[$variableName]->handle(true);
				}
			}
		}

		if($haveFoundHandlerMethod ===  false)
		{
			self::renderHttpStatusPage(404, $selectorType, $selectorName);

			return false;
		}

		return true;
	}

	/**
	 * Calls the handler method for the class.
	 *
	 * @param boolean $handlerMethodExists Pass true to force the method to think an handler method exists. This is used internally when multiple Cayuga class instances exist to prevent erroneous 404 HTTP status codes from being sent.
	 */
	public function handle(bool $handlerMethodExists = false) : void
	{
		switch($this->selectorType)
		{
			case "class": $this->renderHandler("class", $handlerMethodExists); break;
			case "id": $this->renderHandler("id", $handlerMethodExists); break;
			case "tag": $this->renderHandler("tag", $handlerMethodExists); break;
			case "attribute": $this->renderHandler("attribute", $handlerMethodExists); break;
			default:
				self::renderHttpStatusPage(404, $this->selectorType, $this->selectorName);
		}
	}

	/**
	 * Combined getter and setter. If a class handler is passed, the property will be set, otherwise it will be gotten.
	 */
	public function classClass(?object $class = null) : ?object
	{
		return
			  is_null($class)
			? $this->classClass
			: $this->classClass = $class;
	}

	/**
	 * Combined getter and setter. If an id handler is passed, the property will be set, otherwise it will be gotten.
	 */
	public function idClass(?object $id = null) : ?object
	{
		return
			  is_null($id)
			? $this->idClass
			: $this->idClass = $id;
	}

	/**
	 * Combined getter and setter. If a tag handler is passed, the property will be set, otherwise it will be gotten.
	 */
	public function tagClass(?object $tag = null) : ?object
	{
		return
			  is_null($tag)
			? $this->tagClass
			: $this->tagClass = $tag;
	}

	/**
	 * Combined getter and setter. If an attribute handler is passed, the property will be set, otherwise it will be gotten.
	 */
	public function attributeClass(?object $attribute = null) : ?object
	{
		return
			  is_null($attribute)
			? $this->attributeClass
			: $this->attributeClass = $attribute;
	}

	protected function populatePayload() : void
	{
		/*
		Cayuga front-end developers may choose to send the payload property using a GET
		method. The front-end library places that data into the query string as the
		"content" field as a base64 encoded value. On the server, regardless of where
		the data is sent, it will be available in the payload property.
		*/
		if(
			$this->requestMethod == "GET" &&
			isset($_GET["content"]) &&
			is_string($_GET["content"]) // Can only decode the content if it's a string.
		)
		{
			$this->payload
				= $_GET["content"] == ""
				? null
				: base64_decode($_GET["content"]);
		}
		else
		{
			$this->payload = file_get_contents("php://input");

			if(isset($_SERVER["CONTENT_TYPE"]))
			{
				if($_SERVER["CONTENT_TYPE"] == "application/x-www-form-urlencoded")
				{
					$this->variables = new \stdClass();

					foreach($_POST as $field=>$value)
					{
						$this->variables->{$field} = $value;
					}
				}
				// If the Content-Type header is application/json, then the payload needs to be deserialized.
				elseif($_SERVER["CONTENT_TYPE"] == "application/json")
				{
					$this->payload = json_decode($this->payload, false);
				}
			}
			else
			{
				$this->payload = $this->payload == "" ? null : $this->payload;
			}

		}
	}

	protected function populateEventSource() : void
	{
		/*
		Unlike a similar conditional block above, this check should not check for a GET
		method. Instead, the eventSource always need to be looked for in the GET method.

		Another thing to note is that, while Cayuga will always pass the eventSource as
		a string, the check for a string type satisfies the static analysis. It's a good
		check just the same.
		*/
		if(isset($_GET["eventSource"]) && is_string($_GET["eventSource"]))
		{
			$this->eventSource = json_decode(base64_decode($_GET["eventSource"]), false);
		}

		if(isset($_SERVER["PATH_INFO"]))
		{
			$expandedPath = explode("/", trim($_SERVER["PATH_INFO"], "/"));

			$this->selectorType = $expandedPath[0] ?? null;
			$this->selectorName = $expandedPath[1] ?? null;
		}
	}

	protected function redirect(string $location) : void
	{
		$this->headers["Cayuga-Redirect"] = $location;
		$this->send();
	}

	private function renderHandler(string $selectorType, bool $handlerMethodExists = false) : void
	{
		// TODO: If the return is a stream or a handle, then a recursive output would be called for.

		$echoJsResult = null;
		$returnJsResult = null;
		$echoResult = null;
		$returnResult = null;

		// If there is neither a JavaScript handler call or a PHP handler, then send a
		// 404. Only one or the other should not be considered a missing handler.
		// if((is_null($methodName) || $methodName == "") && (is_null($methodNameJs) || $methodNameJs == ""))
		if(!$handlerMethodExists)
		{
			self::renderHttpStatusPage(404, $this->selectorType, $this->selectorName);

			return;
		}

		/* ------------------------------------------------------------------- Render JS
		The JS handler may echo content, or it may return it. Echoed content supersedes
		returned content.
		*/
		if(!is_null($this->methodNameJs) && $this->definedJsInSeparateClass)
		{
			ob_start();
			$returnJsResult = $this->{"$selectorType" . "Class"}->{$this->methodNameJs}($this);
			$echoJsResult = ob_get_clean();
		}
		elseif(!is_null($this->methodNameJs) && $this->methodNameJs !== "")
		{
			ob_start();
			$returnJsResult = $this->{$this->methodNameJs}();
			$echoJsResult = ob_get_clean();
		}

		/*
		If content will be sent from the handler, the handler should return void.
		Sending content to be rendered and returning content to be rendered doesn't make
		any sense. The caller should do one or the other.

		It may be the case that the handler was intended to output empty "content." In
		this case do not usurp it with the return value. The only way to know that an
		empty output was intended is if nothing is returned either. If nothing is
		rendered, and there is no return, just output empty string.

		The rest of the progression is to favor rendered content in the handler, and
		then the return value.
		*/
		if(
			$echoJsResult === "" &&
			($returnJsResult === "" || is_null($returnJsResult))
		)
		{
			$this->js = "";
		}
		elseif($echoJsResult !== "")
		{
			$this->js = $echoJsResult;
		}
		else
		{
			$this->js = $returnJsResult;
		}

		if(!is_null($this->js))
		{
			$this->headers["Cayuga-JavaScript"] = strlen($this->js);
		}

		$this->send();

		/* -------------------------------------------------------------- Render Content
		The handler may echo content, or it may return it. Echoed content supersedes
		returned content.

		If the handlers are in separate classes, then $this instance is passed as a
		parameter.
		*/
		if(!is_null($this->methodName) && $this->definedInSeparateClass)
		{
			ob_start();
			$returnResult = $this->{"$selectorType" . "Class"}->{$this->methodName}($this);
			$echoResult = ob_get_clean();
		}
		elseif(!is_null($this->methodName))
		{
			ob_start();
			$returnResult = $this->{$this->methodName}();
			$echoResult = ob_get_clean();
		}

		/*
		If content will be sent from the handler, the handler should return void.
		Sending content to be rendered and returning content to be rendered doesn't make
		any sense. The caller should do one or the other.

		It may be the case that the handler was intended to output empty "content." In
		this case do not usurp it with the return value. The only way to know that an
		empty output was intended is if nothing is returned either. If nothing is
		rendered, and there is no return, just output empty string.

		The rest of the progression is to favor rendered content in the handler, and
		then the return value.
		*/
		if(
			$echoResult === "" &&
			($returnResult === "" || is_null($returnResult))
		)
		{
			echo "";
		}
		elseif($echoResult !== "")
		{
			echo $echoResult;
		}
		else
		{
			echo $returnResult;
		}
	}

	private function fetchHandlerMethodName(string $selectorType, bool $lookForJs = false) : ?string
	{
		$lookingForMethodName = $this->selectorName ?? "";
		/*
		The process of looking for handler methods is the same for both regular
		handlers and JavaScript handlers. This variable allows an appropriate suffix to
		be added to handler name references, both when looking for the methods'
		existence and determining their names.
		*/
		$jsSuffix = $lookForJs ? "Js" : "";

		/* ------------------------------------------------------------- Extended Cayuga

		This is looking for methods added by extending the Cayuga class.

		Looking for method by selector name.
		*/
		if(method_exists($this, $lookingForMethodName . $jsSuffix))
		{
			$methodName = $lookingForMethodName . $jsSuffix;
			$this->foundHandlerMethod = true;
		}
		// Looking for method by selector name but scoped to HTML attribute type, class, id, et cetera.
		elseif(method_exists($this, $selectorType . ucfirst($lookingForMethodName) . $jsSuffix))
		{
			// Method names may be "scoped" by the selector type (id, class, et cetera).
			$methodName = $selectorType . ucfirst($lookingForMethodName) . $jsSuffix;
			$this->foundHandlerMethod = true;
		}
		// ---------------------------------------------------- Implementation Instances
		elseif(
			!is_null($this->{$selectorType . "Class"}) &&
			method_exists($this->{$selectorType . "Class"}, $lookingForMethodName . $jsSuffix))
		{
			$methodName = $lookingForMethodName . $jsSuffix;
			$this->foundHandlerMethod = true;

			if($jsSuffix === "") $this->definedInSeparateClass = true;
			else $this->definedJsInSeparateClass = true;
		}
		elseif(
			!is_null($this->{$selectorType . "Class"}) &&
			method_exists($this->{$selectorType . "Class"}, $selectorType . ucfirst($lookingForMethodName) . $jsSuffix))
		{
			$methodName = $selectorType . ucfirst($lookingForMethodName) . $jsSuffix;
			$this->foundHandlerMethod = true;
			$this->definedJsInSeparateClass = true;

			if($jsSuffix === "") $this->definedInSeparateClass = true;
			else $this->definedJsInSeparateClass = true;
		}

		return $methodName ?? null;
	}

	/**
	 * Send HTTP headers and and JavaScript there may be.
	 */
	private function send() : void
	{
		if(!isset($this->headers))
		{
			return;
		}

		foreach($this->headers as $header => $value)
		{
			header("$header: $value");
		}

		$this->sendCorsHeaders();

		if(isset($this->js))
		{
			echo $this->js;
		}
	}

	/*
	TODO: It would be a good idea to better implement this based on some details in
	      posts like these:

	      https://stackoverflow.com/questions/10636611/how-does-access-control-allow-origin-header-work
	      https://stackoverflow.com/questions/13400594/understanding-xmlhttprequest-over-cors-responsetext/13400954#13400954

	Some work needs to be done to determine if the OPTIONS preflight request method
	needs to be handled.
	*/
	private function sendCorsHeaders() : void
	{
		/*
		FIXME: Look at the following to know where to find the allowOrigin sites.

		       $this->definedJsInSeparateClass
		       $this->definedInSeparateClass

		If either of these is true, then pull the allow origin sites from the separate
		class.
		*/

		if(count($this->allowOrigin) > 0)
		{
			$allowedOrigins = implode(", ", array_map(
				function($entry) : string {return trim($entry);},
				$this->allowOrigin
			));

			header("Access-Control-Allow-Origin: " . $allowedOrigins);

			if($allowedOrigins != "*")
			{
				header("Vary: Origin");
			}
		}
	}

	protected static function renderHttpStatusPage(int $statusCode, ?string $selectorType = null, ?string $selectorName = null, ?string $eventSource = null) : void
	{
		$message  = "No handler for the event tied to:\r\r";
		$message .= "<blockquote><strong>$selectorType = \"$selectorName\"</strong></blockquote>";
		$pageContent = file_get_contents(__DIR__ . "/templates/httpStatus/" . (string) $statusCode . ".html");

		$pageContent = str_replace("{httpStatus:message}", $message, $pageContent);

		http_response_code($statusCode);

		echo $pageContent;

		// include __DIR__ . "/templates/httpStatus/" . (string) $statuCode . ".html";
	}
}
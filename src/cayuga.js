function Cayuga(register, server) {
	// --------------------------------------------------------- Public declarations
	this.register = register;
	this.server = server || window.location.href + "/cayuga.php";
	/*
	The debounce feature allows successive requests to the server to be throttled in
	order to prevent needless round trips to the server. For some elements, like
	buttons, repeated events may not be an issue and it would be a better experience
	to have the UI updated right away.

	For these elements a threshold can be set. In the case of a button, the
	threshold may be set to 1, in which case the click will be handled right away,
	and other events within the debounce time will be ignored.  For an element like
	a text box, something like 3 or 4 events might make more sense.

	It's worth noting that it's almost impossible for a user to trigger enough
	events such that the JavaScript engine can't respond to them.  As such, in order
	to allow an initial update to the UI and then throttle the requests, a default
	threshold time is set to 250 milliseconds. Most any value less than 250 will be
	handled by the JS engine, anything higher than 250 will create a sense of
	needless lag.
	*/
	this.debounce = {};
	this.debounceDefault = 300; // Milliseconds
	this.debounceThreshold = 0;
	this.debounceThresholdTime = 200;
	this.eventsLibrary = {};
	this.occurrence; // This is set to whatever element the event occurred from.

	// -------------------------------------------------------- Private declarations
	this.private = {
		/*
		These properties are part of the library, but are not explicitly defined here
		since they are accessed via getters and setters and have no initial value.

		They are noted here for the sake of the specification.

		firstEventable
		lastEventable
		*/
		firstEventableSet: false, // Used to determine if the setter was used to
		lastEventableSet: false   // force a value.
	};

	// --------------------------------------------------------- Getters and setters
	Object.defineProperties(this, {
		/*
		Dynamically determine the first "eventable" item in the register, unless it has
		been forced with the setter.
		*/
		firstEventable: {
			get: function() {
				if(this.private.firstEventableSet === true) { // If a value was forced via the setter, return it
					return this.private.firstEventable;       // and be done.
				}                                             //

				var selectors = Object.keys(this.register);

				for(let i = 0; i < selectors.length; i ++) {
					if(this.register[selectors[i]].hasOwnProperty("event")) {
						let elements = document.querySelectorAll(selectors[i]);

						if(elements.length > 0) {
							return elements[0];
						}
					}
				}
			},
			set: function(value) {
				this.private.firstEventableSet = true;

				return this.private.firstEventable = value;
			}
		},
		/*
		Dynamically determine the last "eventable" item in the register, unless it has
		been forced with the setter.
		*/
		lastEventable: {
			get: function() {
				if(this.private.lastEventableSet === true) { // If a value was forced via the setter, return it
					return this.private.lastEventable;       // and be done.
				}                                            //

				var selectors = Object.keys(this.register);

				for(let i = 0; i < selectors.length; i ++) {
					if(this.register[selectors[i]].hasOwnProperty("event")) {
						var candidate = selectors[i];
					}
				}

				let elements = document.querySelectorAll(candidate);

				if(elements.length > 0) {
					return elements[elements.length - 1];
				}
			},
			set: function(value) {
				this.private.lastEventableSet = true;

				return this.private.lastEventable = value;
			}
		}
	});

	/*
	THE SITUATION

	The property below (eventsLibrary) exists for the sole purpose of adding and
	removing event listeners to the elements identified in the register.

	Like so many things in JavaScript that are so much more difficult than they need
	to be, adding and removing event listeners is a royal pain. On the surface they
	seem fine. Call a method to add the listener, pass it the type of event, and
	the handler callback to fire when the event happens.

	Confoundedly, removing the event listener requires the same function to be
	passed to the method that removes the event listener, that was used when adding
	it. That's right! In order to differentiate one event on an element from
	another, JavaScript uses the callback function as a key.

	Rather than using an arbitrarily assigned key to identify one event on an object
	from another, it uses the callback function. At first, it doesn't seem so bad.
	Use a named function, and just refer to the same function name when removing the
	event listener. The problem comes when the named function is a method on a
	prototype that refers to this in its body. When the named callback is fired, it
	has no notion of this, and throws an error.

	The final gripe, and this one makes no sense, at all.  The event handler
	callback can only accept one argument, and it's passed by default. When the
	callback is fired, the browser will pass the event to the callback. That's it!

	This means that there is no way to pass other information handler callbacks
	might need, and in effect, they become little self-contained functions that know
	nothing other than themselves.

	In practice, it doesn't take long to realize how crippling this limitation is.

	THE SOLUTION

	The solution to these limitations has been to create another registry called the
	eventsLibrary that is populated with a callback function for each Cayuga
	register entry.

	This does two things.

	First, it allows a handler callback to be used that "knows" about the this
	scope.

	Second, it allows a way to refer to a handler callback by "name," even thought
	that name is really a field in the eventsLibrary object.
	*/

	// ---------------------------------------------------------------- Initializing
	this.registerEvents();

	if((typeof register.branding === "undefined") || !register.branding) {
		this.brand(); // If the branding shows up, then everything should have loaded fine.
	}
}

/**
 * This is the main method that initializes the Cayuga data structure.
 *
 * This method will scan the entire structure to add events to respond to the various controls in the application.
 *
 * NOTE: This is a utility function for the internals of Cayuga, and it's assumed that this method will never be called from an application.
 */
Cayuga.prototype.registerEvents = function() {
	this.unregisterEvents();

	/*
	Grabbing the keys of the register. These map to selectors like #something,
	.something, or tags like div.
	*/
	var selectors = Object.keys(this.register);

	/*
	Going over all the selectors in the registration. That is, the top-level nodes
	in the registration.
	*/
	for(let s = 0; s < selectors.length; s ++) {
		var selectorName = selectors[s];

		if(this.register[selectorName].hasOwnProperty("event")) {
			this.addEventListeners(selectorName);
		}
	}
}

/**
 * This is the main method that un-initializes the Cayuga data structure.
 *
 * This method will scan the entire structure to remove events from the various controls in the application.
 *
 * NOTE: This is a utility function for the internals of Cayuga, and it's assumed that this method will never be called from an application.
 */
Cayuga.prototype.unregisterEvents = function() {
	/*
	Grabbing the keys of the register. These map to selectors like #something,
	.something, or tags like div.
	*/
	var selectors = Object.keys(this.register);

	/*
	Going over all the selectors in the registration. That is, the top-level nodes
	in the registration.
	*/
	for(let s = 0; s < selectors.length; s ++) {
		var selectorName = selectors[s];

		if(this.register[selectorName].hasOwnProperty("event")) {
			this.removeEventListeners(selectorName);
		}
	}
}

/**
 * Given the name of a selector item, this will add the event listener for each instance of the element.
 *
 * @param string The name of the register item the items belong to.
 */
Cayuga.prototype.addEventListeners = function(selectorName) {
	var elements = document.querySelectorAll(selectorName);

	for(let e = 0; e < elements.length; e ++) {
		/*
		This is a round-about way to add an event listener such that copies of the
		handler function will be used instead of references. The pass-by-reference
		nature of JavaScript is causing only the handler of the last iteration of the
		register to be used.
		*/
		this.addEventListener(elements[e], selectorName);
	}
}

/**
 * Add an event listener to each instance of the element.
 *
 * @param object The element the event listener is to be added to.
 * @param string The selector name (reference/field name).
 */
Cayuga.prototype.addEventListener = function(element, selectorName) {
	var self = this;

	/*
	This is adding a function that calls the Cayuga.run() method when the
	control event takes place. It's added to a register of sorts so that the
	target selector can be referenced. It's a whole big thing with JavaScript
	events!

	See the lengthy note at the top of this file where the Cayuga.eventsLibrary
	property is introduced.

	The short of it is, that the registry is used because when an event is
	removed, the callback serves as the ID of the event to be removed.

	The code just below is the callback to be added as the event handler, but
	the core of it is the reference to the run() method.

	This callback is ultimately added in the last few lines.
	*/
	this.eventsLibrary[selectorName] = function(){
		self.occurrence = element;

		if(self.debounce[selectorName] > 0) { // This counter is used to "absorb" repeated event calls.
			self.debounce[selectorName] += 1; // Each call increments this counter, while the run()
		}                                     // method decrements it, and runs it when the counter is
		else {                                // zero again.
			self.debounce[selectorName] = 1;  //
		}                                     //

		var timeout;

		if(
			self.debounce[selectorName] <=
			(self.register[selectorName].debounceThreshold || self.debounceThreshold)
		) {
			timeout = self.debounceThresholdTime;
		}
		else if(typeof self.register[selectorName].debounce === "undefined") { // The debounce can be turned off by setting selector.debounce = 0.
			timeout = self.debounceDefault;                                    // This handles a debounce value of 0 so that it's not assumed to
		}                                                                      // be missing, but should actually be considered as turned off.
		else {                                                                 //
			timeout = self.register[selectorName].debounce;                    //
		}                                                                      //

		window.setTimeout(
			function(element, selector, selectorName) {
				self.run(element, selector, selectorName);
			},
			timeout,
			element, self.register[selectorName], selectorName
		)
	};

	element.addEventListener(
		self.register[selectorName].event, // The event to add.
		this.eventsLibrary[selectorName]); // The function to add.
}

/**
 * Given the name of a selector item, this will remove the event listener for each instance of the element.
 *
 * @param string The name of the register item the items belong to.
 */
Cayuga.prototype.removeEventListeners = function(selectorName) {
	var elements = document.querySelectorAll(selectorName);

	for(let e = 0; e < elements.length; e ++) {
		this.removeEventListener(elements[e], selectorName);
	}
}

/**
 * Remove an event listener from each instance of the element.
 *
 * @param object The element the event listener is to be added to.
 * @param string The selector name (reference/field name).
 */
Cayuga.prototype.removeEventListener = function(element, selectorName) {
	element.removeEventListener(this.register[selectorName].event, this.eventsLibrary[selectorName]);

	delete this.eventsLibrary[selectorName];
}

/**
 * This is the thing that needs to run in response to an event. It is added by the wrapper method, this.addEventListener().
 */
Cayuga.prototype.run = function(eventSource, selector, selectorName) {
	var self = this;

	self.debounce[selectorName] -= 1;     // Discarding multiple events.
                                          //
	if(self.debounce[selectorName] > 0) { //
		return; // Sending to /dev/null.  //
	}                                     //

	var httpRequest = new XMLHttpRequest();

	if(typeof selector.sendCookies !== "undefined") {         // This is necessary if the request is supposed to send
		httpRequest.withCredentials = !!selector.sendCookies; // cookie data to the remote server.
	}                                                         //

	if(typeof selector.runBefore === "function") {            // Running this here so the function has the chance to
		selector.runBefore.call(this, eventSource, selector); // access all parts of the selector, before anything else
	}                                                         // happens to them. To truly "run before."

	// ------------------------------------------------------------------- Re-direct
	/*
	If the redirect property is specified, then no attempt will be made to call a
	handler, either remote or client-side. A simple window.location assignment will
	be made and the run call will be returned.
	*/
	if(selector.redirect) {
		if(typeof selector.redirect == "function") {
			window.location = selector.redirect.call(this, eventSource, selector);
		}
		else {
			window.location = selector.redirect;
		}

		return;
	}

	// ---------------------------------------------------------------- Downloadable
	/*
	Assume the case where the result of clicking a button for example, prompts the
	download of some tab-delimited data, or a binary file. Setting the downloadable
	property tells CayugaJS to redirect the browser to the URI of the content to be
	downloaded. Otherwise, it will be written to an outlet, which it not useful.

	See the notes on the onreadystatechange() definition below for how re-directs
	are handled.
	*/
	else if(selector.downloadable) {
		var downloadWorkspace = document.createElement("a");

		downloadWorkspace.setAttribute("id", eventSource.id + "_DownloadWorkspace");
		downloadWorkspace.setAttribute("download", "");

		if((selector.downloadable === true) && (selector.uri)) {
			downloadWorkspace.setAttribute("href", self.server);
		}
		else if(typeof selector.downloadable === "function") {
			downloadWorkspace.setAttribute("href", selector.downloadable.call(this, eventSource, selector));
		}
		else {
			downloadWorkspace.setAttribute("href", selector.downloadable);
		}

		document.body.appendChild(downloadWorkspace);
		downloadWorkspace.click();
		document.body.removeChild(downloadWorkspace);

		/*
		NOTE: If we return here, and the URI is a re-direct, nothing will happen. This
              situation will be handled later. Do not return here.
		*/
	}

	// -------------------------------------------------------------- Client handler
	/*
	If the handler property is a function, then the intent is to handle the event in
	the client. This means that the XHR is should not be run. The uri property is
	not necessary and will be ignored if present.
	*/
	else if(typeof selector.handler === "function") {
		selector.handler.call(this, eventSource, selector);

		if(typeof selector.runAfter === "function") {
			selector.runAfter.call(this, eventSource, selector);
		}

		return;
	}

	// An element on the page will be updated locally, no need for a trip to the server.
	else if(this.localBinding(eventSource, selector)) {
		return;
	}

	httpRequest.onreadystatechange = function() {
		if(httpRequest.readyState === XMLHttpRequest.DONE) {
			var response = self.parseHttpResponse(httpRequest);

			/*
			In order to accommodate all the manners in which a request to the server can be
			made, and the types of responses that can come back, there is a very specific
			checkdown CayugaJS will make to ensure the most comprehensive and flexible
			execution.
			*/

			// ------------------------------------------------------------ No event handler
			if(httpRequest.status === 404) {
				let helperMessage =  "Cayuga: It looks like there was no handler for the event.\n\n";
				    helperMessage += "\t• It may be the case in which the handler is only JavaScript. If so, the simplest solution is to implement an event handler that simply returns null.\n";
				    helperMessage += "\t• It's worth noting that if the intent was to render only JavaScript, that JavaScript will still be executed in spite of the 404 status code.\n";
					helperMessage += "\nHave a look at the dump below."

				console.warn(helperMessage);
				console.warn({
					uri: this.server,
					inlet: selector.inlet || "",
					outlet: selector.outlet || "",
					allowedStatusCodes: selector.allowedStatusCodes,
					status: httpRequest.status,
					statusText: httpRequest.statusText,
					content: httpRequest.responseText.length > 500 ? httpRequest.responseText.substring(0, 500) + "…" : httpRequest.responseText
				});

				if(response.javaScript) {
					window.eval(response.javaScript); // This needs to be second in case the JS needs to act on the newly rendered HTML from the response.content.
				}
			}
			/* ------------------------------------------------------------------- Re-direct

			CORS issues are too easy to encounter and demand cooperation from owners of
			other domains in order to work reliably. In order to work around this, HTTP
			status 303 codes are not used to determine if the browser should load a new
			document. Instead, a typical 200 code is expected along with a custom Cayuga
			header. The custom header tells Cayuga to load a new document.
			*/
			else if((httpRequest.status === 200) && response.redirect) {
				if(response.javaScript) {
					window.eval(response.javaScript);
				}

				window.location = response.redirect;
			}
			// ----------------------------------------------- Downloadable has been handled
			else if((httpRequest.status === 200) && (typeof selector.downloadable !== "undefined")) {
				/*
				Do nothing, this is ultimately handled prior to this point, and is only here for
				the cases where the URI to download is actually a re-direct. Without it, these
				conditions would fall all the way through, and the error message in the else
				block would be output.

				If an event loads a downloadable resource, but is re-directed, perhaps for
				authentication the download will fail. The execution will eventually reach this
				condition which is here simply to avoid the error at the bottom of the block.

				The re-direct still needs to be handled though, so a URI will be resolved just
				below this onreadystatechange definition. It will try to load the downloadable
				URI again so as to process the re-direct that got us in this situation.
				*/

				// TODO: Does this need to render response.javaScript like the other conditionals?
			}

			// -------------------------------------------------------- Conventional control
			else if(                                                                     // This whole block is only necessary if the content is going to be
				(typeof selector.outlet !== "undefined") &&                              // written to the page. So, this is the first check. Then we need
				(                                                                        // to have an acceptable HTTP status code returned from the server.
					(httpRequest.status === 200) ||                                      // Code 200 is the expected value, but the allowedStatusCodes array
					(                                                                    // allows content coming from the server to be rendered in events
						(typeof selector.allowedStatusCodes !== "undefined") &&          // where the HTTP status code from the server is not a 200. For
						(selector.allowedStatusCodes.indexOf(httpRequest.status) !== -1) // example, to allow an error message to be rendered after a failed
					)                                                                    // authentication attempt (status code 403).
				)                                                                        //
			) {                                                                          //
				if(response.content) {
					self.render(self.outlet(eventSource, selector), response.content);
				}

				if(response.javaScript) {
					window.eval(response.javaScript); // This needs to be second in case the JS needs to act on the newly rendered HTML from the response.content.
				}
			}
			// -------------------------------------------------------- Problematic response
			else {
				let helperMessage =  "Cayuga: There was a problem with the request. The issue may be one of the following:\n\n";
				    helperMessage += "\t• The HTTP status code is not permitted to render content. Use the allowedStatusCodes array to add allowed codes.\n";
					helperMessage += "\t• The URI may have been requesting a cross origin address.\n";
					helperMessage += "\t• The content from the server is not suitable for rendering to a page element, for example text data, or a binary file. Use the downloadable property to have the browser download the content.\n";
					helperMessage += "\nHave a look at the dump below."

				console.warn(helperMessage);
				console.warn({
					uri: this.server,
					inlet: selector.inlet || "",
					outlet: selector.outlet || "",
					allowedStatusCodes: selector.allowedStatusCodes,
					status: httpRequest.status,
					statusText: httpRequest.statusText,
					content: httpRequest.responseText.length > 500 ? httpRequest.responseText.substring(0, 500) + "…" : httpRequest.responseText
				});
			}

			document.addEventListener("DOMContentLoaded", function(){}); // Re-firing this event in case anything
			document.dispatchEvent(new Event("DOMContentLoaded"));       // is watching this event.

			/*
			Since the request has likely changed what elements are in the DOM, some events
			are stale, and some events need to be attached to the new elements. This call
			should handle that.
			*/
			self.update();

			if(typeof selector.runAfter === "function") {            // Running after XHR. This happens here to skirt
				selector.runAfter.call(this, eventSource, selector); // the asynchronous nature of the XHR so that it
			}                                                        // actually runs after.
		}
	};

	var uri;

	if(typeof selector.downloadable === "function") {
		uri = selector.redirect.call(this, eventSource, selector);
	}
	else if(typeof selector.downloadable !== "undefined") {
		uri = selector.downloadable;
	}
	else {
		if(selectorName.substring(0, 1) == "#") {
			uri = self.server + "/id/" + selectorName.substring(1);
		}
		else if(selectorName.substring(0, 1) == ".") {
			uri = self.server + "/class/" + selectorName.substring(1);
		}
		else if(
			selectorName.substring(0, 1) == "[" &&
			selectorName.substring(selectorName.length - 1) == "]"
		) {
			uri = self.server + "/attribute/" + selectorName.substring(1, selectorName.length - 1);
		}
		else {
			uri = self.server + "/tag/" + selectorName;
		}
	}

	/*
	The checkdown of where to look for a payload. This is useful to allow the
	payload to be set simply from something like an input tag without the need to
	explicitly set an inlet or a payload property.

		1.  Check for something in the payload property first
		2.  Then the inlet
		3.  Finally just use the value or innerHTML of the element that triggered the event

	If the server is responding to something like a button click instead of passing
	content from a text box, it makes more sense from the perspective of an HTTP
	request to be sent as a GET method. Data submitted from a textbox or input
	field are likely POSTs.

	If the payload property is set, the request will be submitted as a POST.
	*/
	var requestContent = "";
	var method
		= typeof selector.method !== "undefined"
		? selector.method.toUpperCase()
		: (
			(typeof selector.inlet !== "undefined") ||
			(typeof selector.fields !== "undefined") ||
			(typeof selector.payload !== "undefined")
				? "POST"
				: "GET"
		);

	/*
	Adding the eventSource to the request to the server. This will create an object
	consisting of the attributes on the element that tripped the event. The object
	will be serialized as JSON, base64 encoded, and sent in the query string.
	*/
	var eventSourceAttributes = {};

	if(eventSource.attributes.length > 0) {
		for(var eventSourceIndex = 0; eventSourceIndex < eventSource.attributes.length; eventSourceIndex ++) {
			if(eventSource.attributes.hasOwnProperty(eventSourceIndex)) {
				eventSourceAttributes[eventSource.attributes[eventSourceIndex].name] = eventSource.attributes[eventSourceIndex].value
			}
		}
	}

	// Determining where to find the content.
	if(typeof selector.payload != "undefined" || typeof selector.fields != "undefined") {
		requestContent = this.createPayload(eventSource, selector);
	}
	else if(typeof selector.inlet != "undefined") {
		requestContent = this.getContent(this.inlet(eventSource, selector));
	}
	// If the content is still unknown, send the whole element that originated the event.
	else {
		requestContent = this.getContent(eventSource);

		if(typeof requestContent == "object") {
			requestContent = "";
		}

		/*
		If we're in this block and the requestContent is not "", then we're probably
		sending the whole element. This also means that the requestContent could be too
		large to send as a GET, so a POST will automatically be used, just to be safe.
		*/
		if(requestContent != "") {
			method = "POST";
		}
	}

	// It makes more sense to send content for GETs as a parameter than as a request body.
	if(method == "GET") {
		uri += "?content=" + btoa(requestContent);
		uri += "&eventSource=" + btoa(JSON.stringify(eventSourceAttributes));

		httpRequest.open(method, uri, true);
		httpRequest.setRequestHeader("Content-Type", selector.contentType || "text/plain");
		httpRequest.send();
	}
	else {
		uri += "?eventSource=" + btoa(JSON.stringify(eventSourceAttributes));

		httpRequest.open(method, uri, true);

		// If there is a fields array, assume it's a conventional form body.
		if(typeof selector.fields != "undefined") {
			httpRequest.setRequestHeader("Content-Type", selector.contentType || "application/x-www-form-urlencoded");
		}
		else if(typeof selector.payload != "undefined") {
			// If there's a payload, check to see if it's an array or an object.
			if(selector.payload instanceof Array || selector.payload instanceof Object) {
				httpRequest.setRequestHeader("Content-Type", selector.contentType || "application/json");
				requestContent = JSON.stringify(requestContent);
			}
			// The fall back is that the payload is plain text.
			else {
				httpRequest.setRequestHeader("Content-Type", selector.contentType || "text/plain");
			}
		}

		httpRequest.send(requestContent);
	}
}

/**
 * Create a payload to send to the UI server.
 *
 * If the details object has a payload property, then that will just be returned as is. Otherwise it Looks for an array of fields to assemble a payload from.
 *
 * The payload may also be an array or an object. In which case, the content will be serialized as JSON.
 *
 * The payload property may be a function as well.
 *
 * @param object selector The registration node.
 * @return string
 */
Cayuga.prototype.createPayload = function(eventSource, selector) {
	if(selector.payload) {
		if(typeof selector.payload === "function") {
			return selector.payload.call(this, eventSource, selector);
		}

		return selector.payload;
	}

	/*
	TODO: The first thing that should be sought is if the fields value is actually
	      a form. If so, then all the fields of the form should be used.
	      Otherwise, individual fields can be used instead.

	      The original intent was that the payload could be constructed from an
	      array of field names. It may be a good idea to allow the "fields"
	      property to actually be a form, and the whole form can be scanned for
	      fields.

	      This will need to be a separate suite of methods in order to handle
	      radio buttons, multi-select lists, and checkboxes.
	*/

	// A payload can be a list of fields, an array, an object, or plain text.
	if(selector.fields instanceof Array) {
		let payload = [];
		let fieldValue = null;

		for(let fieldName of selector.fields) {
			fieldName = fieldName.replace(/[\#]/g, "");
			fieldValue = document.querySelector('#' + fieldName);

			if(fieldValue) {
				fieldValue = fieldValue.value || fieldValue.innerHTML;
				payload.push(fieldName + '=' + encodeURIComponent(fieldValue));
			}
		}

		return payload.join("&");
	}

	if(selector.payload instanceof Array || selector.payload instanceof Object) {
		return JSON.stringify(selector.payload);
	}

	return selector.payload;
}

/**
 * If target is an object or an array it scans the object looking for HTML elements to update with the help of the this.setContent() method.
 *
 * @param object selector The registration node.
 * @return void
 */
Cayuga.prototype.render = function(target, content) {
	if(target === null) {
		return;
	}

	if((typeof target === "object") || (typeof target === "array")) {
		if(target.length) {
			for(var i = 0; i < target.length; i ++) { // Handles the case where there are multiple
				this.setContent(target[i], content);  // elements to replace the contents of.
			}                                         //
		}
		else {
			this.setContent(target, content);
		}
	}
}

/**
 * Replaces the content of an HTML element. If no element is found at target, it's just ignored.
 *
 * @param object selector The registration node.
 * @return string The content from the UI server call.
 */
Cayuga.prototype.setContent = function(target, content) {
	/*
	WARN: It should be noted that this is a stopgap fix and may cause other issues.
          Take for example the case in which only the value of an attribute should
          be changed, but the innerHTML should stay the same. The case in which
          this might be necessary can't be described now, but it may happen at some
		  point.

	Some elements have attributes whether they're explicitly set in the HTML or not.
	For example, an INPUT tag has an innerHTML attribute, and a BUTTON tag has a
	value attribute.

	For this reason, we can't look at just one attribute and then the next in order
	to update the element. Rather, both value and innerHTML properties need to be
	updated.

	For now, we'll leave it like this and a more accommodating solution will be
	introduced if need be. It might be the case that we need an additional
	property on the details object.
	*/
	if(typeof target.value === "string") {
		target.value = content;
	}

	if(typeof target.innerHTML === "string") {
		target.innerHTML = content;
	}

	return content
}

/**
 * Fetch the value of the target object. The idea is to simplify access to a form element's value or its innerHTML.
 *
 * @param object selector The registration node.
 * @return string The value the target pointed to
 */
Cayuga.prototype.getContent = function(target) {
	if((typeof target === "undefined") || (target === null)) {
		return;
	}

	/*
	Apparently, even elements like INPUTs have an innerHTML property. Then again
	elements like button will report a value property even if one isn't explicitly
	set. To address this, a checkdown was decided on, which will be to look for a
	value first, so long as it's not an empty string, then innerHTML, but it can't
	be an empty string either.
	*/
	if(typeof target.value === "string" && target.value !== "") {
		return target.value;
	}

	if(typeof target.innerHTML === "string" && target.innerHTML !== "") {
		return target.innerHTML;
	}

	return target;
}

/**
 * The inlet property should always be one of, a query selector, a tag reference, or a function.
 *
 * If an inlet is a string value that doesn't match any tags, the value will be returned.
 *
 * @param object eventSource The element the event originated from.
 * @param any selector The object itself from the data structure, or a query selector for the DOM. Otherwise, it could be a simple value.
 * @return any The form element referenced from the query selector, the return value from a function call, or the value assigned to the inlet.
 */
Cayuga.prototype.inlet = function(eventSource, selector) {
	if((selector.inlet === null) || (selector === null)) {
		console.warn("Cayuga:  An inlet property has been set, but its value is null.  Consider removing the inlet property.");

		return;
	}

	var inlet = selector.inlet || selector;

	if(typeof inlet === "function") {
		return inlet.call(this, eventSource, inlet);
	}
	else if(!(document.querySelector(inlet) === null)) {
		return document.querySelector(inlet);
	}

	return inlet;
}

/**
 * The outlet property should always be one of, a query selector, a tag reference, or a function.
 *
 * If an outlet is a string value that doesn't match any tags, the value will be returned.
 *
 * @param object eventSource The element the event originated from.
 * @param any selector The object itself from the data structure, or a query selector for the DOM. Otherwise, it could be a simple value.
 * @return any The form element referenced from the query selector, the return value from a function call, or the value assigned to the outlet.
 */
Cayuga.prototype.outlet = function(eventSource, selector) {
	if((selector.outlet === null) || (selector === null)) {
		console.warn("Cayuga:  An outlet property has been set, but its value is null.  Consider removing the outlet property.");

		return;
	}

	var outlet = selector.outlet || selector;

	if(typeof outlet === "function") {
		return outlet.call(this, eventSource, selector);
	}
	else if(!(document.querySelector(outlet) === null)) { // If there's one, then it's safe to return using querySelectorAll() in case
		return document.querySelectorAll(outlet);         // there is more than one instance. This is helpful for when the selector
	}                                                     // is a tag.

	return outlet;
}

Cayuga.prototype.localBinding = function(eventSource, selector) {
	if(
		(typeof selector.client !== "undefined") &&
		(selector.client == true) &&
		(typeof selector.inlet !== "undefined") &&
		(typeof selector.outlet !== "undefined")
	) {
		var inlet = this.inlet(eventSource, selector);
		var outlet = this.outlet(eventSource, selector);

		if(typeof inlet === "string") {
			this.render(outlet, inlet);
		}
		else {
			this.render(outlet, inlet.value || inlet.innerHTML || null);
		}

		return true;
	}

	return false;
}

/**
 * Add a new selector.
 *
 * This is part of the CRUD API (C-create) to manage changes to the register.
 *
 * @param string selectorName A string reference to the register object to be updated.
 * @param object details The details to updated with.
 */
Cayuga.prototype.add = function(selectorName, details) {
	if(this.register[selectorName]) {
		console.warn("The selector already exists.  Use Cayuga.update() to makes changes to items that already exists");
		return;
	}

	// Nothing to do.
	if(!details.hasOwnProperty("event")) {
		return;
	}

	this.register[selectorName] = details;
	this.addEventListeners(selectorName);
}

/*
FIXME: These CRUD operations really need to be re-written to act on the data
       structure rather than the registry.
*/

/**
 * Get a selector.
 *
 * This is part of the CRUD API (R-read) to manage changes to the register.
 *
 * @param string selectorName A string reference to the register object to be retrieved.
 */
Cayuga.prototype.get = function(selectorName) {
	if(selectorName) {
		return this.register[selectorName];
	}
}

/**
 * Remove a selector.
 *
 * This is part of the CRUD API (D-delete) to manage changes to the register.
 *
 * @param string selectorName A string reference to the register object to be removed.
 */
Cayuga.prototype.remove = function(selectorName) {
	if(selectorName) {
		this.removeEventListeners(selectorName);

		delete this.register[selectorName];
	}
}

/**
 * Update a selector.
 *
 * This is part of the CRUD API (U-update) to manage changes to the register.
 *
 * If no arguments are passed, then the whole registry will be reloaded.
 *
 * @param string selectorName A string reference to the register object to be updated. The default is false, which means the whole registry will be updated.
 */
Cayuga.prototype.update = function(selectorName = false) {
	if(selectorName) {
		this.removeEventListeners(selectorName);
		this.addEventListeners(selectorName);

		return;
	}
	else {
		// Unregistering events will prune the events for elements that are no longer present.
		this.unregisterEvents();
	}

	this.registerEvents();
}

/**
 * Tell the browser to load a new URI.
 *
 * This is callable from a Cayuga instance by-way-of this.redirect(LOCATION). However, it cannot be called in this manner from inside an arrow function, because arrow functions do not have the proper scope for "this."
 *
 * @param string location
 */
Cayuga.prototype.redirect = function(location) {
	window.location = location;
}

Cayuga.prototype.brand = function() {
	console.log(
		"\n\n\n" +
		"   {}\n" +
		"()()()()\n" +
		" ()()()   C  A  Y  U  G  A\n" +
		"  ()()\n" +
		"   ()\n" +
		"\n\n\n"
	);
}

Cayuga.prototype.parseHttpResponse = function(httpRequest) {
	var response = {
		content: null,
		javaScript: null,
		redirect: null
	};

	try {
		// INFO: response versus responseText is a subtle difference.
		if(httpRequest.getResponseHeader("Cayuga-Redirect")) {
			response.redirect = httpRequest.getResponseHeader("Cayuga-Redirect");
		}

		if(httpRequest.getResponseHeader("Cayuga-JavaScript")) {
			response.javaScript = httpRequest.responseText.substring(0, httpRequest.getResponseHeader("Cayuga-JavaScript"));
			response.content = httpRequest.responseText.substring(httpRequest.getResponseHeader("Cayuga-JavaScript"));
		}
		else {
			response.content = httpRequest.responseText;
		}
	}
	catch(error) {
		let message = "Cayuga:  There was trouble parsing the response from the server.";

		console.error(message);
	}

	return response;
}
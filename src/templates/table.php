<table id="TABLE" class="TABLE">
	<thead>
		<tr>
			<th>HEADER_1</th>
			<th>HEADER_2</th>
			<th>HEADER_3</th>
		</tr>
	</thead>
	<tbody>
		<?php
		/** @psalm-suppress InvalidScope */
		if(isset($this)):
		foreach($this->bindings->ROWS as $row): ?>
		<tr>
			<td><?= $row["COLUMN_1"] ?></td>
			<td><?= $row["COLUMN_2"] ?></td>
			<td><?= $row["COLUMN_3"] ?></td>
		</tr>
		<?php
		endforeach;
		endif;
		?>
	</tbody>
	<tfoot>
		<tr>
			<th>FOOTER_1</th>
			<th>FOOTER_2</th>
			<th>FOOTER_3</th>
		</tr>
	</tfoot>
</table>
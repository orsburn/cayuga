<div id="CONTROL" class="CONTROL">
	<?php
	/*
	Psalm is complaining about $this being used outside of a class context. There no
	way for it to know that this is a template that will be used in another file in
	which $this will be defined.

	Unless another way to render values from $this can be figured out, the Psalm
	suppress docblock will be used. Note that the PHP echo tags cannot be used with
	the Psalm docblocks
	*/

	/** @psalm-suppress InvalidScope */
	echo $this ? $this->binding->VALUE : "";
	?>
</div>
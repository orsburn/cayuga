# Change Log

Trying to follow this guide.

https://keepachangelog.com/

## [Unreleased]

## [1.8.2] YYYY-MM-DD

### Added

- Form fields are now available in a ```variables``` property.
- The attributes of the element the event was fire from are available in an ```eventsSource``` property.

### Changed

- Payloads can now be sent as plain text, form fields, an array, or an object.